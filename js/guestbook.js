var url = "https://script.google.com/macros/s/AKfycbzWQeANEAxrIrHOpZRjhruykTQ4iY268tBkDE0S9n2F8O81z2s/exec";

// Post Data
function post() {
    var message = getInput();
    write(message);
    show(new Date().toLocaleString(), message);
    textarea.value = "";
}

// Get Input
function getInput() {
    var textarea = document.querySelector("#myTextarea");
    if(textarea.value.trim() === ""){
        alert('it\'s empty.');
        return;
    }
    return textarea.value.replace(/\r\n|\r|\n/g,"<br />");
}

// Show Data
function show(date, message) {
    var commentTemplate = document.querySelector('.comment-template');
    var newComment = commentTemplate.cloneNode(true);

    newComment.setAttribute("style", "");
    newComment.querySelector(".avatar > img").setAttribute("src", "https://api.adorable.io/avatars/80/anonymous_avatar"+Math.random()+".png");
    newComment.querySelector(".content > .metadata > .date").innerHTML = date;
    newComment.querySelector(".content > .text").innerHTML = message;

    var messageBoard = document.querySelector("#messageBoard");
    messageBoard.prepend(newComment);
}

// Write Data
function write(message) {
    var formdata = new FormData();
    formdata.append("method", "write");
    formdata.append("message", message);
    
    var requestOptions = {
        method: 'POST',
        mode: 'no-cors',
        body: formdata,
    };
    
    fetch(url, requestOptions);
}

// Read All Data
function readAll() {
    var formdata = new FormData();
    formdata.append("method", "readAll");
    
    var requestOptions = {
        method: 'POST',
        body: formdata,
    };
    
    fetch(url, requestOptions)
        .then(response => response.text())
        .then(result => {
            var allData = JSON.parse(result);
            for (let i = 0; i < allData.length; i++) {
                const element = allData[i];
                var dateString = new Date(element.datetime).toLocaleString();
                show(dateString, element.message);
            }
            return allData;
        })
        .catch(error => console.log('error', error));

}


document.addEventListener('DOMContentLoaded', readAll);

var postBtn = document.querySelector("#postBtn");
var textarea = document.querySelector("#myTextarea");
postBtn.addEventListener('click', post)
textarea.addEventListener('keypress', function (e) {
    if (e.key === 'Enter' && !e.shiftKey) {
        e.preventDefault();
        post()
    }
});



